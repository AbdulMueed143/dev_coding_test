package au.com.ceo.android_test.models.entitymodels

import java.util.*

class TotalTransactionAmount {
    var amount : Float = 0.0f
    var clientNumber : String = ""
    var productGroupCode: String = ""
    var transactionDate: Date? = null
}