package au.com.ceo.android_test.database.converters

import androidx.room.TypeConverter
import java.util.*

class DateTimeConverter {

    companion object {

        @TypeConverter
        @JvmStatic
        fun fromTimestamp(value: Long) : Date? {
            val calendar =  Calendar.getInstance()
            calendar.timeInMillis = value
            return calendar.time
        }

        @TypeConverter
        @JvmStatic
        fun toTimestamp(date: Date) : Long {
            val calendar =  Calendar.getInstance()
            calendar.time = date
            return calendar.timeInMillis
        }

    }

}