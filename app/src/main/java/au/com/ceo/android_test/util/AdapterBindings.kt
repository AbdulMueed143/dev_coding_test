package au.com.ceo.android_test.util

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import au.com.ceo.android_test.adapters.TransactionsListAdapter
import au.com.ceo.android_test.models.entitymodels.TotalTransactionAmount
import au.com.ceo.android_test.models.entitymodels.Transaction


@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Transaction>?) {
    items?.let {
        (listView.adapter as TransactionsListAdapter).notifyDataSetChanged()
    }
}

@BindingAdapter("app:items")
fun setItemsTotalTransactionAmount(listView: RecyclerView, items: List<TotalTransactionAmount>?) {
    items?.let {
        (listView.adapter as TransactionsListAdapter).notifyDataSetChanged()
    }
}