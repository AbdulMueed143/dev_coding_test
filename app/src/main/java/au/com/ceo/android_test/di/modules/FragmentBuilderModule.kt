package au.com.ceo.android_test.di.modules

import au.com.ceo.android_test.views.reports.DailySummaryReportFragment
import au.com.ceo.android_test.views.transactions.TransactionsFragment
import au.com.ceo.android_test.views.transactions.TotalTransactionAmountFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributesTotalTransactionAmountFragment() : TotalTransactionAmountFragment

    @ContributesAndroidInjector
    abstract fun contributesDailySummaryReportFragment() : DailySummaryReportFragment

    @ContributesAndroidInjector
    abstract fun contributesAllTrasactionsFragment() : TransactionsFragment

}