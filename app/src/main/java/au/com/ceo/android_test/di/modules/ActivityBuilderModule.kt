package au.com.ceo.android_test.di.modules

import au.com.ceo.android_test.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributesMainActivity() : MainActivity
}