package au.com.ceo.android_test.util

import android.content.Context
import androidx.lifecycle.MutableLiveData
import au.com.ceo.android_test.models.entitymodels.Transaction

object CustomFileManager {

    fun readFile(context: Context) : List<Transaction> {

        val transaction = ArrayList<Transaction>()

        val bufferReader = context.assets.open(Constants.FILE_NAME).bufferedReader().use {
            it.readText()
        }

        bufferReader.split("\n").forEach {
            transaction.add(Transaction.Builder(it).build())
        }

        return transaction
    }

}