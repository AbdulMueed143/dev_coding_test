package au.com.ceo.android_test.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import au.com.ceo.android_test.models.entitymodels.TotalTransactionAmount
import au.com.ceo.android_test.models.entitymodels.Transaction
import io.reactivex.Flowable
import kotlinx.coroutines.flow.Flow

@Dao
interface TransactionsDao : BaseDao<Transaction> {

    //transaction for the client mate!
    @Query("SELECT * FROM "+Transaction.tableName +" WHERE clientNumber=:id")
    fun getTransactionsForClientFlow(id: String) : Flow<List<Transaction>>

    //Summary report
    @Query("SELECT * FROM "+Transaction.tableName)
    fun getDailySummaryFlow() : Flow<List<Transaction>>

    //The Business user would like to see the TOTAL Transaction Amount of each UNIQUE product they have done for the DAY
    @Query("SELECT SUM(quantityLong - quantityShort) amount, clientNumber, productGroupCode, transactionDate  FROM "+Transaction.tableName+" group by clientNumber, productGroupCode, transactionDate ")
    fun getTransactionTotalAmountFlow() : Flow<List<TotalTransactionAmount>>
}