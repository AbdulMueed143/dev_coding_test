package au.com.ceo.android_test.views.transactions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import au.com.ceo.android_test.adapters.TransactionsListAdapter
import au.com.ceo.android_test.databinding.FragmentTransactionsBinding
import au.com.ceo.android_test.di.ViewModelProviderFactory
import au.com.ceo.android_test.models.viewmodels.TransactionsViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class TransactionsFragment : DaggerFragment() {

    companion object {
        const val name = "TransactionsFragment"
    }

    @Inject
    lateinit var viewModelProviderFactory : ViewModelProviderFactory
    private lateinit var viewModel : TransactionsViewModel
    private var _binding : FragmentTransactionsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity(), viewModelProviderFactory).get(TransactionsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionsBinding.inflate(inflater, container, false).apply {
            transactionViewModel = viewModel
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    fun setupObservers() {
        viewModel.transactionsFor1234.observe(viewLifecycleOwner, {
            binding.rcyTransactions.adapter?.notifyDataSetChanged()
        })
    }

    private fun setupRecyclerView() {
        val vm = binding.transactionViewModel
        vm?.let {
            binding.rcyTransactions.adapter = TransactionsListAdapter(it)
        } ?: kotlin.run {
            //Else say something mate!
        }
    }
}