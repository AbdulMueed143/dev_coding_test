package au.com.ceo.android_test.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import au.com.ceo.android_test.databinding.TotalTransactionAmountItemBinding
import au.com.ceo.android_test.models.entitymodels.TotalTransactionAmount
import au.com.ceo.android_test.models.viewmodels.TransactionsViewModel

//All transactions but we have default filter so we will just apply that.. 1234
class TotalTransactionAmountAdapter(private val transactionListViewModel: TransactionsViewModel) : RecyclerView.Adapter<TotalTransactionAmountAdapter.ViewHolder>()
{

    class ViewHolder(val binding: TotalTransactionAmountItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: TransactionsViewModel, item: TotalTransactionAmount?) {
            binding.totalTransactionAmountModel = item
            binding.transactionViewModel = viewModel
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = TotalTransactionAmountItemBinding.inflate(inflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = transactionListViewModel.totalTransactionsAmount.value?.get(position)
        holder.bind(transactionListViewModel, item)
    }

    override fun getItemCount(): Int = transactionListViewModel.totalTransactionsAmount.value?.size ?: 0

    override fun getItemViewType(position: Int): Int {
        return position
    }
}