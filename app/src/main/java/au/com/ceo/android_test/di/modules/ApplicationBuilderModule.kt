package au.com.ceo.android_test.di.modules

import android.app.Application
import android.content.Context
import au.com.ceo.android_test.database.dao.TransactionsDao
import au.com.ceo.android_test.datasource.local.TransactionLocalDataSource
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationBuilderModule {

    @Singleton
    @Provides
    fun provideGlideInstance(application: Application, requestOptions: RequestOptions) : RequestManager {
        return Glide.with(application).setDefaultRequestOptions(requestOptions)
    }

    //Get the remote resource
    @Singleton
    @Provides
    fun providesTransactionLocalDatasource(application: Application, transactionsDao: TransactionsDao) :
            TransactionLocalDataSource = TransactionLocalDataSource(application.applicationContext, transactionsDao)
}