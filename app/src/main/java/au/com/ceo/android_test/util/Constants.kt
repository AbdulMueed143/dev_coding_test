package au.com.ceo.android_test.util

object Constants {
    const val DATEFORMATE = "yyyyMMdd"
    const val FILE_NAME = "Input.txt"
    const val DATABASE_VERSION = 1
    const val DATABASE_NAME = "TransactionsApp"
}