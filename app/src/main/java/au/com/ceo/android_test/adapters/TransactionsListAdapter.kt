package au.com.ceo.android_test.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import au.com.ceo.android_test.databinding.TransactionItemBinding
import au.com.ceo.android_test.models.entitymodels.Transaction
import au.com.ceo.android_test.models.viewmodels.TransactionsViewModel

//All transactions but we have default filter so we will just apply that.. 1234
class TransactionsListAdapter(private val transactionListViewModel: TransactionsViewModel) : RecyclerView.Adapter<TransactionsListAdapter.ViewHolder>()
{

    class ViewHolder(val binding: TransactionItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: TransactionsViewModel, item: Transaction?) {
            binding.transactionModel = item
            binding.transactionViewModel = viewModel
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = TransactionItemBinding.inflate(inflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = transactionListViewModel.transactionsFor1234.value?.get(position)
        holder.bind(transactionListViewModel, item)
    }

    override fun getItemCount(): Int = transactionListViewModel.transactionsFor1234.value?.size ?: 0

    override fun getItemViewType(position: Int): Int {
        return position
    }
}