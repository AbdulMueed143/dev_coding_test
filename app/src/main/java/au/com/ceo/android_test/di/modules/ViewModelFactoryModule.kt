package au.com.ceo.android_test.di.modules

import androidx.lifecycle.ViewModelProvider
import au.com.ceo.android_test.di.ViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(providerFactory: ViewModelProviderFactory) : ViewModelProvider.Factory
}