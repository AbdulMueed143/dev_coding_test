package au.com.ceo.android_test.datasource.local

import android.content.Context
import androidx.lifecycle.LiveData
import au.com.ceo.android_test.database.dao.TransactionsDao
import au.com.ceo.android_test.models.entitymodels.Transaction
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TransactionLocalDataSource @Inject constructor(val context: Context, val transactionsDao: TransactionsDao) {

    fun getTransactionsForClientFlow(id: String)  = transactionsDao.getTransactionsForClientFlow(id)

    fun getDailySummaryFlow() = transactionsDao.getDailySummaryFlow()

    fun getTransactionTotalAmountFlow() = transactionsDao.getTransactionTotalAmountFlow()

}