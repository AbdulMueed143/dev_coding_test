package au.com.ceo.android_test.di.modules

import androidx.lifecycle.ViewModel
import au.com.ceo.android_test.di.annotations.ViewModelKey
import au.com.ceo.android_test.models.viewmodels.TransactionsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelBuilderModule {

    @Binds
    @IntoMap
    @ViewModelKey(TransactionsViewModel::class)
    abstract fun bindTransactionsViewModel(albumListViewModel: TransactionsViewModel) : ViewModel

}