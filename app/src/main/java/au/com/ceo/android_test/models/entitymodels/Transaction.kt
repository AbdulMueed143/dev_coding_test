package au.com.ceo.android_test.models.entitymodels

import androidx.room.*
import au.com.ceo.android_test.util.Converters
import com.google.gson.annotations.SerializedName
import java.util.*

//Each line consists of 176 charachters, and each item is subsstring from that string
//The best way to deserialise this one would be to have builder class for the sake of clarity
@Entity(tableName = Transaction.tableName)
data class Transaction(
    @ColumnInfo(name = "id")
    @PrimaryKey
    var id : String = UUID.randomUUID().toString(),

    //we could either use range or the length of each to break the transaction string
    @ColumnInfo(name = "recordCode")
    var recordCode : String = "", //1-3

    @ColumnInfo(name = "oppositePartyCode")
    var oppositePartyCode : String = "", //20-25

    @ColumnInfo(name = "currencyCode")
    var currencyCode : String = "", //46-48

    @ColumnInfo(name = "movementCode")
    var movementCode : String = "", //49-50

    @ColumnInfo(name = "buySellCode")
    var buySellCode : String = "", //51-51

    @ColumnInfo(name = "quantityLongSign")
    var quantityLongSign : String = "", //52-52

    @ColumnInfo(name = "quantityLong")
    var quantityLong : Float = 0.0f, //53-62

    @ColumnInfo(name = "quantityShortSign")
    var quantityShortSign : String = "", //63-63

    @ColumnInfo(name = "quantityShort")
    var quantityShort : Float =  0.0f, //64-73

    @ColumnInfo(name = "brokerFeeDEC")
    var brokerFeeDEC : Float = 0.0f, //74-85 - 2 decimals

    @ColumnInfo(name = "brokerFeeDC")
    var brokerFeeDC : String = "", //86-86

    @ColumnInfo(name = "brokerFeeCurCode")
    var brokerFeeCurCode : String = "", //87-89

    @ColumnInfo(name = "clearingFeeDEC")
    var clearingFeeDEC : Float = 0.0f, //90-101 - 2 decimals

    @ColumnInfo(name = "clearingFeeDC")
    var clearingFeeDC : String = "", //102-102

    @ColumnInfo(name = "clearingFeeCurCode")
    var clearingFeeCurCode : String = "", //103-105

    @ColumnInfo(name = "commission")
    var commission : Float = 0.0f, //106-117 - 2 decimals

    @ColumnInfo(name = "commissionDC")
    var commissionDC : String = "", //118-118

    @ColumnInfo(name = "commissionCurCode")
    var commissionCurCode : String = "", //119-121

    @ColumnInfo(name = "transactionDate")
    var transactionDate : Date = Date(), //122-129 - CCYYMMDD

    @ColumnInfo(name = "futureReference")
    var futureReference : String = "", //130-135

    @ColumnInfo(name = "ticketNumber")
    var ticketNumber : String = "", //136-141

    @ColumnInfo(name = "externalNumber")
    var externalNumber : String = "", //142-147

    @ColumnInfo(name = "transactionPrice")
    var transactionPrice : Float = 0.0f, //148-162 - 7 decimals

    @ColumnInfo(name = "traderInitials")
    var traderInitials : String = "", //163-168

    @ColumnInfo(name = "oppositeTraderId")
    var oppositeTraderId : String = "", //169-175

    @ColumnInfo(name = "openCloseCode")
    var openCloseCode : String = "", //176-176

    @ColumnInfo(name = "filler")
    var filler : String = "", //177-303 - not available - ignore

    @Embedded
    var client: Client?,

    @Embedded
    var product: Product?
) {

    @Ignore
    var getClientInformation : String = ""
        get() {
            return "${client?.clientNumber + "\n" +client?.clientType}"
        }

    @Ignore
    var getProductInformation : String = ""
        get() {
            return "${product?.productGroupCode + "\n" +product?.symbol}"
        }

    @Ignore
    var getTransactionInformation : String = ""
        get() {
            return "${recordCode + "\n" + externalNumber+ "\n" +quantityShortSign}"
        }

    companion object {
        const val tableName = "Transactions"
    }


    data class Builder(val transactionContent: String) {
        fun build() = Transaction(
            recordCode = transactionContent.substring(0,3),
            oppositePartyCode = transactionContent.substring(19,25),
            currencyCode = transactionContent.substring(45, 48),
            movementCode = transactionContent.substring(48,50),
            buySellCode = transactionContent.substring(50,51),
            quantityLongSign = transactionContent.substring(51,52),
            quantityLong = Converters.stringToNumber(transactionContent.substring(52,62)),
            quantityShortSign = transactionContent.substring(62,63),
            quantityShort = Converters.stringToNumber(transactionContent.substring(63,73)),
            brokerFeeDEC = Converters.stringToNumberWithDecimals(transactionContent.substring(73,85), 2),
            brokerFeeDC = transactionContent.substring(85,86),
            brokerFeeCurCode = transactionContent.substring(86,89),
            clearingFeeDEC = Converters.stringToNumberWithDecimals(transactionContent.substring(89,101) , 2 ),
            clearingFeeDC = transactionContent.substring(101,102),
            clearingFeeCurCode = transactionContent.substring(102,105),
            commission = Converters.stringToNumberWithDecimals(transactionContent.substring(105, 117),2),
            commissionDC = transactionContent.substring(117,118),
            commissionCurCode = transactionContent.substring(118,121),
            transactionDate = Converters.stringToDate(transactionContent.substring(121,129)),
            futureReference = transactionContent.substring(129,135),
            ticketNumber = transactionContent.substring(135,141),
            externalNumber = transactionContent.substring(141, 147),
            transactionPrice = Converters.stringToNumberWithDecimals(transactionContent.substring(147, 162),7),
            traderInitials = transactionContent.substring(162, 168),
            oppositeTraderId = transactionContent.substring(168, 175),
            openCloseCode = transactionContent.substring(175, 176),

            client = Client.Builder(transactionContent).build(),
            product = Product.Builder(transactionContent).build()
        )
    }
}