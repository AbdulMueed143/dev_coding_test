package au.com.ceo.android_test.database

import android.annotation.SuppressLint
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import au.com.ceo.android_test.database.converters.DateTimeConverter
import au.com.ceo.android_test.database.dao.TransactionsDao
import au.com.ceo.android_test.models.entitymodels.Transaction
import au.com.ceo.android_test.util.Constants
import au.com.ceo.android_test.util.CustomFileManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

@Database(entities = [Transaction::class], version = Constants.DATABASE_VERSION, exportSchema = false)
@TypeConverters(DateTimeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun transactionsDao() : TransactionsDao

    companion object {

        private var instance : AppDatabase? = null

        fun getInstance(context: Context) : AppDatabase {

            if (instance == null) {
                synchronized(AppDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, Constants.DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .addCallback(object : RoomDatabase.Callback() {
                            @SuppressLint("CheckResult")
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)

                                Executors.newSingleThreadExecutor().execute {
                                    //lets read the file and save data in this database
                                    val instance = getInstance(context)
                                    instance.transactionsDao()
                                        .insertAll(CustomFileManager.readFile(context.applicationContext))
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({},{})

                                }
                            }
                        })
                        .build()
                }
            }

            return instance!!
        }
    }
}