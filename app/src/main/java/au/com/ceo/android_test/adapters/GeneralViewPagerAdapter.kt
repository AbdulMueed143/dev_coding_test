package au.com.ceo.android_test.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import java.lang.IllegalArgumentException

class GeneralViewPagerAdapter(fragmentManager: FragmentManager,
                              private val images: Boolean = false,
                              var context: Context
) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
{

    private val fragmentList  = mutableMapOf<Int, Fragment>()
    private val fragmentTitleList = mutableMapOf<Int, String>()
    private val fragmentDrawableMap: MutableMap<Int, Drawable> = mutableMapOf()
    private var intrinsicHeight = 0
    private var intrinsicWidth = 0

    override fun getItem(position: Int): Fragment {
        return fragmentList.get(position) ?: Fragment()
    }

    override fun getCount(): Int {
        return fragmentList.keys.size
    }


    override fun getPageTitle(position: Int): CharSequence? {

        if (!hasImages())
            return fragmentTitleList.get(position)
        else {

            fragmentDrawableMap.get(position)?.setBounds(0,0, intrinsicWidth , intrinsicHeight)

            return ""
//            return fragmentDrawableMap.get(position).toString().toUpperCase()


//            val imageSpan = ImageSpan(fragmentDrawableMap.get(position)!!, ImageSpan.ALIGN_BASELINE)
//            val spannableString = SpannableStringBuilder()
//            spannableString.setSpan(imageSpan, 0, 0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//            return spannableString
        }

    }

    fun addFragment(pos: Int, fragment: Fragment, fragmentTitle: String, fragmentIcon: Int = -1) {

        fragmentList.put(pos, fragment)
        fragmentTitleList.put(pos, fragmentTitle)
        if (fragmentIcon != -1) {
            var position = fragmentDrawableMap.size
            val draw = ContextCompat.getDrawable(context, fragmentIcon)
            intrinsicHeight = draw?.intrinsicHeight!!
            intrinsicWidth = draw.intrinsicWidth
            fragmentDrawableMap.put(position, draw)
        }
    }

    fun titleExists(title : String) : Boolean {

        fragmentTitleList.values.forEach {
            if(it.equals(title, ignoreCase = true))
                return true
        }

        return false
    }

    fun getFragment(position: Int) : Fragment {
        if (position > fragmentList.size)
            throw IllegalArgumentException("Your position is greater than number of fragments in list")

        return fragmentList.get(position) ?: Fragment()
    }

    fun getFragments() : MutableMap<Int, Fragment> {
        return fragmentList
    }

    fun hasImages(): Boolean {
        return images
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        fragmentList[position] = fragment
        return fragment
    }

}