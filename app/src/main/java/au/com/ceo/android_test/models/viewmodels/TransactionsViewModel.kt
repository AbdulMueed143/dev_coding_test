package au.com.ceo.android_test.models.viewmodels

import android.util.Log
import androidx.lifecycle.*
import au.com.ceo.android_test.database.repositories.TransactionRepository
import au.com.ceo.android_test.models.entitymodels.TotalTransactionAmount
import au.com.ceo.android_test.models.entitymodels.Transaction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * We do not need multiple view models, one because we want to share data
 * Second because we do not want to over engineer simple project like this one
 * */
class TransactionsViewModel @Inject constructor(transactionRepository: TransactionRepository) : ViewModel() {

    //Total_Transaction_Amount should be a Net Total of the
    //(QUANTITY LONG - QUANTITY SHORT) values for each client per product
    private val transactionsForClient1234Flow = transactionRepository.getTransactionsForClientFlow("1234")
    val transactionsFor1234 : MutableLiveData<List<Transaction>> = MutableLiveData()

    private val totalTransactionsAmountFlow = transactionRepository.getTransactionTotalAmountFlow()
    val totalTransactionsAmount : MutableLiveData<List<TotalTransactionAmount>> = MutableLiveData()

    init {

        //Since our functionality is very basic we do not need anything more than this..
        viewModelScope.launch {
            collectTotalTransactionAmount()
        }

        viewModelScope.launch {
            collectTransactions()
        }

        //similarly we will add one call for summary report...
    }

    private suspend fun collectTransactions() = transactionsForClient1234Flow.collectLatest {
        transactionItems -> kotlin.run {
            transactionsFor1234.postValue(transactionItems)
        }
    }

    private suspend fun collectTotalTransactionAmount() = totalTransactionsAmountFlow.collectLatest {
        ttAmount -> kotlin.run {
            totalTransactionsAmount.postValue(ttAmount)
        }
    }

}