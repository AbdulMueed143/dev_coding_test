package au.com.ceo.android_test.util

import android.annotation.SuppressLint
import android.os.Build
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

//I have not added any checks for now, currently since we have standard file we are assuming its
//best case scenario where everything is great, its an elysium
object Converters {

    //TODO check if it is of approriate length and can be converted to datetime before trying
    @SuppressLint("SimpleDateFormat", "WeekBasedYear")
    fun stringToDate(content: String) : Date {
        val simpleDateFormat = SimpleDateFormat(Constants.DATEFORMATE)
        return simpleDateFormat.parse(content) ?: Date()
    }

    //TODO check if the length of content is greater than decimal count and also other checks to make sure it is doable task
    fun stringToNumberWithDecimals(content: String, decimalCount: Int) : Float {
        val afterDot = content.substring(content.length - decimalCount - 1, content.length - 1)
        val beforeDot = content.substring(0, content.length - decimalCount - 2)
        return "$beforeDot.$afterDot".toFloat()
    }

    fun stringToNumber(content: String) : Float {
        if( content.isNullOrBlank() )
            return 0.0f

        return content.toFloat()
    }

}