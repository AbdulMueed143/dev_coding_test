package au.com.ceo.android_test.database.repositories

import au.com.ceo.android_test.datasource.local.TransactionLocalDataSource
import javax.inject.Inject

//Should be application context
class TransactionRepository @Inject constructor(private val localDataSource : TransactionLocalDataSource) {

    //Well if we need to incorporate network call or other data sources we would do it here
    fun getTransactionsForClientFlow(id: String) = localDataSource.getTransactionsForClientFlow(id)

    fun getDailySummaryFlow() = localDataSource.getDailySummaryFlow()

    fun getTransactionTotalAmountFlow() = localDataSource.getTransactionTotalAmountFlow()

}