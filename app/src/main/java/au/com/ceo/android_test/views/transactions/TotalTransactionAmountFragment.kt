package au.com.ceo.android_test.views.transactions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import au.com.ceo.android_test.adapters.TotalTransactionAmountAdapter
import au.com.ceo.android_test.databinding.FragmentTotalTransactionAmountBinding
import au.com.ceo.android_test.di.ViewModelProviderFactory
import au.com.ceo.android_test.models.viewmodels.TransactionsViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class TotalTransactionAmountFragment : DaggerFragment() {

    companion object {
        const val name = "TotalTransactionAmountFragment"
    }

    @Inject
    lateinit var viewModelProviderFactory : ViewModelProviderFactory
    private lateinit var viewModel : TransactionsViewModel
    private var _binding : FragmentTotalTransactionAmountBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity(), viewModelProviderFactory).get(TransactionsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTotalTransactionAmountBinding.inflate(inflater, container, false).apply {
            transactionViewModel = viewModel
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    fun setupObservers() {
        viewModel.totalTransactionsAmount.observe(viewLifecycleOwner, {
            binding.rcyTotalTransactionsAmount.adapter?.notifyDataSetChanged()
        })
    }

    private fun setupRecyclerView() {
        val vm = binding.transactionViewModel
        vm?.let {
            binding.rcyTotalTransactionsAmount.adapter = TotalTransactionAmountAdapter(it)
        } ?: kotlin.run {
            //Else say something mate!
        }
    }
}