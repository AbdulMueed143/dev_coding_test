package au.com.ceo.android_test.models.entitymodels

//Product Information
class Product(
    var productGroupCode : String = "", //26-27
    var exchangeCode : String = "", //28-31 d
    var symbol : String = "", //32-37
    var expirationDate : String = "" //38-45 - CCYYMMDD
) {

    data class Builder(val transactionContent : String) {
        fun build() = Product(
            productGroupCode = transactionContent.substring(25,27),
            exchangeCode = transactionContent.substring(27,31),
            symbol = transactionContent.substring(31,37),
            expirationDate = transactionContent.substring(37,45)
        )
    }

}