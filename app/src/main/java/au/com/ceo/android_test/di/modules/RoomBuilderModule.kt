package au.com.ceo.android_test.di.modules

import android.app.Application
import au.com.ceo.android_test.database.AppDatabase
import au.com.ceo.android_test.database.dao.TransactionsDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomBuilderModule {

    @Singleton
    @Provides
    fun providesAppDatabase(application: Application) : AppDatabase {
        return AppDatabase.getInstance(application.applicationContext)
    }

    @Singleton
    @Provides
    fun providesTransactionsDao(appDatabase: AppDatabase) : TransactionsDao {
        return appDatabase.transactionsDao()
    }

}