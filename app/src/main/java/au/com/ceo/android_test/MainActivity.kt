package au.com.ceo.android_test

import android.os.Bundle
import android.view.MenuItem
import androidx.viewpager.widget.ViewPager
import au.com.ceo.android_test.adapters.GeneralViewPagerAdapter
import au.com.ceo.android_test.views.reports.DailySummaryReportFragment
import au.com.ceo.android_test.views.transactions.TransactionsFragment
import au.com.ceo.android_test.views.transactions.TotalTransactionAmountFragment
import com.google.android.material.navigation.NavigationBarView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity(),
    NavigationBarView.OnItemSelectedListener,
    ViewPager.OnPageChangeListener {

    private val TOOLBAR_ELEVATION = 4.0f
    private var previousMenuItem: MenuItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.elevation = TOOLBAR_ELEVATION
        supportActionBar?.title = ""

        bottomNavigationBar.setOnItemSelectedListener(this)
        viewPager.addOnPageChangeListener(this)

        setupViewPager()
    }

    private fun setupViewPager() {

        val generalViewPagerAdapter = GeneralViewPagerAdapter(supportFragmentManager, false, this)

        generalViewPagerAdapter.addFragment(0, TotalTransactionAmountFragment(), TotalTransactionAmountFragment.name)
        generalViewPagerAdapter.addFragment(1, DailySummaryReportFragment(), DailySummaryReportFragment.name)
        generalViewPagerAdapter.addFragment(2, TransactionsFragment(), TransactionsFragment.name)

        viewPager.adapter = generalViewPagerAdapter
        viewPager.offscreenPageLimit = 3
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        if(previousMenuItem != null)
            previousMenuItem!!.setChecked(false)
        else
            bottomNavigationBar.menu.getItem(0).setChecked(false)

        bottomNavigationBar.menu.getItem(position).setChecked(true)
        previousMenuItem = bottomNavigationBar.menu.getItem(position)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when(item.itemId) {

            R.id.transactionsAmount -> {
                viewPager.setCurrentItem(0)
            }

            R.id.summary -> {
                viewPager.setCurrentItem(1)
            }

            R.id.transactions -> {
                viewPager.setCurrentItem(2)
            }
        }

        return false
    }
}