package au.com.ceo.android_test.models.entitymodels

//Client Information
data class Client(
    var clientType : String = "", //4-7
    var clientNumber : String = "", //8-11
    var accountNumber : String = "", //12-15
    var subAccountNumber : String = "" //16-19
){

    data class Builder(val transactionContent: String) {

        fun build() = Client(
            clientType = transactionContent.substring(3,7),
            clientNumber = transactionContent.substring(7,11),
            accountNumber = transactionContent.substring(11,15),
            subAccountNumber = transactionContent.substring(15,19)
        )
    }
}